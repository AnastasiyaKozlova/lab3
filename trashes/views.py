# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from .models import Trash
from .forms import TrashForm
import shutil
import sys
import os
sys.path.insert(0, '/usr/local/lib/python2.7/dist-packages/lab2-1.0-py2.7.egg')
import smart_rm


def list(request):
    trashes = Trash.objects.all()
    for trash in trashes:
        trash.free_space = smart_rm.remove.RemoveCommand(
                                                trash_path=trash.trash_path,
                                                size=trash.max_trash_size
                                                ).find_free_space()
    return render(request, 'trashes/list.html', {'trashes': trashes})


def create(request):
    form = TrashForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        trash = form.save()
        smart_rm.my_trash.create_trash_dir(trash.trash_path)
        return redirect('trashes:list')
    return render(request, 'trashes/new.html', {'form': form})


def update(request, id):
    trash = get_object_or_404(Trash, id=id)
    form = TrashForm(request.POST or None, instance=trash)
    old_path = os.path.abspath(trash.trash_path)
    if form.is_valid() and request.method == 'POST':
        trash = form.save()
        new_path = os.path.abspath(trash.trash_path)
        if new_path != old_path:
            os.renames(old_path, new_path)
        return redirect('trashes:list')
    return render(request, 'trashes/update.html',
                  {'trash_id': id, 'form': form})


def delete(request, id):
    trash = get_object_or_404(Trash, id=id)
    if request.method == 'POST':
        if os.path.exists(trash.trash_path):
            shutil.rmtree(trash.trash_path)
        trash.delete()
    return redirect('trashes:list')


def detail(request, id):
    trash = get_object_or_404(Trash, id=id)
    trash_obj = smart_rm.my_trash.Trash(trash_path=trash.trash_path,
                                        max_file_size=trash.max_file_size,
                                        max_file_time=trash.max_file_time,
                                        max_trash_size=trash.max_trash_size
                                        )
    trash_list = trash_obj.show()
    return render(request, "trashes/detail.html",
                  {'trash_list': trash_list, 'trash_id': id, })


def restore(request, id):
    trash = get_object_or_404(Trash, id=id)
    if request.method == 'POST':
        name = request.POST['file_name']
        smart_rm.restore.RestoreCommand(trash_path=trash.trash_path
                                        ).run([name])
    return redirect(request.META.get('HTTP_REFERER'))


def clear(request, id):
    trash = get_object_or_404(Trash, id=id)
    if request.method == 'POST':
        name = request.POST['file_name']
        smart_rm.clear.ClearCommand(trash_path=trash.trash_path).run([name])
    return redirect(request.META.get('HTTP_REFERER'))
