from django.conf.urls import url

from . import views

app_name = 'trashes'
urlpatterns = [
    url(r'^$', views.list, name='list'),
    url(r'^new/$', views.create, name='create'),
    url(r'^update/(?P<id>\d+?)$', views.update, name='update'),
    url(r'^delete/(?P<id>\d+?)$', views.delete, name='delete'),
    url(r'^detail/(?P<id>\d+?)$', views.detail, name='detail'),
    url(r'^restore/(?P<id>\d+?)$', views.restore, name='restore'),
    url(r'^clear/(?P<id>\d+?)$', views.clear, name='clear'),
]
