# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.db import models


class Trash(models.Model):
    """model for trash."""

    trash_path = models.CharField(max_length=200, default=os.path.expanduser(
                                  "~/.local/share/my_trash"), unique=True)
    max_trash_size = models.IntegerField(default="100000")
    max_file_size = models.IntegerField(default="100")
    max_file_time = models.IntegerField(default="604800")
    free_space = models.IntegerField(default="100000")

    def __str__(self):
        return self.trash_path
