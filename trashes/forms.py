import os
from django import forms
from models import Trash


class TrashForm(forms.ModelForm):
    class Meta:
        model = Trash
        fields = ["trash_path", "max_trash_size", "max_file_size",
                  "max_file_time"]
