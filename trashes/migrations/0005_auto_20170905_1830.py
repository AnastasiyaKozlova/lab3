# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-05 18:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trashes', '0004_auto_20170904_1208'),
    ]

    operations = [
        migrations.AddField(
            model_name='trash',
            name='free_space',
            field=models.IntegerField(default='100000'),
        ),
        migrations.AlterField(
            model_name='trash',
            name='max_file_size',
            field=models.IntegerField(default='100'),
        ),
        migrations.AlterField(
            model_name='trash',
            name='max_file_time',
            field=models.IntegerField(default='604800'),
        ),
        migrations.AlterField(
            model_name='trash',
            name='max_trash_size',
            field=models.IntegerField(default='100000'),
        ),
    ]
