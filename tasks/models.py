from __future__ import unicode_literals

from django.db import models
from trashes.models import Trash

# Create your models here.


class Task(models.Model):
    """model for task"""

    name = models.CharField(max_length=50)
    trash = models.ForeignKey(Trash, on_delete=models.CASCADE,
                              related_name='tasks')
    is_dry_run = models.BooleanField(default=False)

    executed = models.BooleanField(default=False)

    def file_paths(self):
        return [el.path for el in self.files.all()]

    def __str__(self):
        return self.name


class File(models.Model):
    INIT_STATUS = 0
    REMOVED_STATUS = 1
    ERROR_STATUS = 2
    DRY_RUN = 3
    STATUS_CHOICES = (
        (INIT_STATUS, 'Init'),
        (REMOVED_STATUS, 'Complete'),
        (ERROR_STATUS, 'ERROR'),
        (DRY_RUN, 'Dry_Run'),
    )

    path = models.CharField(max_length=200)
    task = models.ForeignKey(Task, on_delete=models.CASCADE,
                             related_name="files",)
    status = models.IntegerField(choices=STATUS_CHOICES, default=INIT_STATUS)
    error_message = models.CharField(max_length=1000, default='')

    @property
    def status_name(self):
        return self.STATUS_CHOICES[self.status][1]

    def __str__(self):
        return "File: id:{0}, path: {1}".format(self.id, self.path)

    class Meta:
        unique_together = ('path', 'task')
