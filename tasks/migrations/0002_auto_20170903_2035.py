# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-03 20:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='status',
        ),
        migrations.AddField(
            model_name='task',
            name='executed',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterUniqueTogether(
            name='file',
            unique_together=set([('path', 'task')]),
        ),
    ]
