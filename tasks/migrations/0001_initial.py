# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-09-03 10:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('trashes', '0002_auto_20170902_1918'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('path', models.CharField(max_length=200)),
                ('status', models.IntegerField(choices=[(0, 'Init'), (1, 'Processing'), (2, 'Complete'), (3, 'ERROR')], default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('status', models.IntegerField(choices=[(0, 'Init'), (1, 'Processing'), (2, 'Complete'), (3, 'ERROR')], default=0)),
                ('trash', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tasks', to='trashes.Trash')),
            ],
        ),
        migrations.AddField(
            model_name='file',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files', to='tasks.Task'),
        ),
    ]
