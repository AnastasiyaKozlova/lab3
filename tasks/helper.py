import os
from django.shortcuts import get_object_or_404
from models import Task


class FileObject(object):
    def __init__(self, base_dir, name, task_id):
        self.name = name
        self.path = os.path.join(base_dir, name)
        self.is_dir = os.path.isdir(self.path)
        self.task_id = task_id

    def belongs_to_task(self):
        task = get_object_or_404(Task, id=self.task_id)
        return self.path in task.file_paths()
