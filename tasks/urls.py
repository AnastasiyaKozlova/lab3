from django.conf.urls import url

from . import views

app_name = 'tasks'
urlpatterns = [
    url(r'^$', views.list, name='list'),
    url(r'^new/$', views.create, name='create'),
    url(r'^update/(?P<id>\d+?)$', views.update, name='update'),
    url(r'^delete/(?P<id>\d+?)$', views.delete, name='delete'),
    url(r'^detail/(?P<id>\d+?)$', views.detail, name='detail'),
    url(r'^detail/(?P<id>\d+?)/tree(?P<path>.*)$', views.get_files, name='get_files'),
    url(r'^detail/(?P<id>\d+?)/execute$', views.execute, name='execute'),
    url(r'^add_file_to_task/(?P<id>\d+?)$', views.add_file_to_task, name='add_file_to_task'),
    url(r'^remove_file_from_task/(?P<id>\d+?)$', views.remove_file_from_task, name='remove_file_from_task'),
    url(r'^add_by_regex/(?P<id>\d+?)$', views.add_by_regex, name='add_by_regex')
]
