# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Task, File
from .forms import TaskForm
import os
from helper import FileObject
import smart_rm
import datetime
from smart_rm.my_rm.common_functions import RemoveMethods


def list(request):
    tasks = Task.objects.all()
    return render(request, 'tasks/list.html', {'tasks': tasks})


def create(request):
    form = TaskForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('tasks:list')
    return render(request, 'tasks/new.html', {'form': form})


def update(request, id):
    task = get_object_or_404(Task, id=id)
    form = TaskForm(request.POST or None, instance=task)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('tasks:list')
    return render(request, 'tasks/update.html', {'form': form, 'task': task})


def delete(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == 'POST':
        task.delete()
        return redirect('tasks:list')


def detail(request, id):
    task = get_object_or_404(Task, id=id)
    files = task.files.all()
    return render(request, 'tasks/detail.html',
                  {'files': files, 'task_id': id})


def get_files(request, id, path):
    FILES_PER_PAGE = 10
    path = path if path else os.path.expanduser('~')
    items = dict()
    try:
        files = os.listdir(path)
        items = map(lambda filename: FileObject(path, filename, id), files)
        paginator = Paginator(items, FILES_PER_PAGE)
        page = request.GET.get('page')

        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)

        return render(request, 'tasks/get_files.html',
                      {'items': items, 'task_id': id,
                       'paginator': paginator, 'current_dir': path})
    except Exception:
        return render(request, 'tasks/error_page.html')


def add_file_to_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == 'POST':
        path = request.POST['path']
        try:
            File.objects.create(path=path, task=task)
        except Exception:
            pass
    return redirect(request.META.get('HTTP_REFERER'))


def remove_file_from_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == 'POST':
        path = request.POST['path']
        file = task.files.filter(path=path).first()
        if file:
            file.delete()
    return redirect(request.META.get('HTTP_REFERER'))


def add_by_regex(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == 'POST':
        data = request.POST
        files = smart_rm.my_rm.common_functions.get_files_by_regex(
            data['regex'], [data['directory']])
        for file in files:
            File.objects.create(path=file, task=task)
        return redirect('tasks:detail', id=task.id)
    else:
        return render(request, 'tasks/regexp_task.html', {'task_id': id})


def execute(request, id):
    task = get_object_or_404(Task, id=id)
    task_files = task.file_paths()
    trash = task.trash
    if task.is_dry_run:
        is_dry_run = True
    else:
        is_dry_run = False
    smart_rm.clear.ClearCommand(trash_path=trash.trash_path,
                                is_dry_run=is_dry_run
                                ).delete_by_size_date(value_date=datetime.timedelta(seconds=trash.max_file_time))
    trashed_files = smart_rm.remove.RemoveCommand(trash_path=trash.trash_path,
                                                  max_file_size=trash.max_file_size,
                                                  size=trash.max_trash_size,
                                                  remove_method=RemoveMethods.RECURSIVE_MODE,
                                                  is_dry_run=is_dry_run
                                                  ).run(task_files)
    task.executed = True
    task.save()

    for trashed_file in trashed_files:
        file_model = task.files.get(path=trashed_file.original_location)
        if trashed_file.state == "OK":
            file_model.status = File.REMOVED_STATUS
            file_model.error_message = ''
        elif trashed_file.state == "DRY_RUN":
            file_model.status = File.DRY_RUN
        else:
            file_model.status = File.ERROR_STATUS
            file_model.error_message = trashed_file.error_message
        file_model.save()

    return redirect('tasks:list')
