# README #

This README document whatever steps are necessary to get this application up and running.

### What is this repository for? ###

This reposutory is about web-app, which implements work with local file system.
What it shell to do:
	1) User can create local trashes, setup its parametres, change it after setup. 
	2) View list of trash's files, possibility to restore file from trash or delete completely.
	3) Create tasks to remove different files to different trashes.

### Requirement ###
	1) python2.7
	2) postgresql 9.3.17
	3) django 1.11.2

### How do I get set up? ###
	The first step is to download smart_rm tools from https://bitbucket.org/AnastasiyaKozlova/python_labs 
and look there how to setup this util.
	After it u need to be sure, that u have postgresql database, if not, use this link to manage this 
https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04
	Then user need to go in root directory of the project and run next commands:  
	1) $source myprojectenv/bin/activate 		#activate virtual environment 
	2) $python manage.py makemigrations
	3) $python manage.py migrate
	4) $python manage.py runserver				#run server